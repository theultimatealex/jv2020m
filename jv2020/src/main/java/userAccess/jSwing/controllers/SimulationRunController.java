package userAccess.jSwing.controllers;

import entitys.Simulation;
import entitys.World;
import jLife.Configuration;
import userAccess.jSwing.views.SimulationRunView;

public class SimulationRunController {
	private int cycles;
	private SimulationRunView simulationView;
	private Simulation simulation;
	private World world;
	
	public SimulationRunController(Simulation simulation) {
		assert simulation != null;
		this.simulation = simulation;
		this.initSimulationRunController();
	}
	
	private void initSimulationRunController() {	
		this.cycles = Integer.parseInt((Configuration.get().getProperty("simulation.defaultCycles")));
		this.world = simulation.getWorld();	
		this.simulationView = new SimulationRunView();
		this.simulationView.pack();
		this.simulationView.setVisible(true);
		this.runSimulation();
	}
	
	public void runSimulation() {
		int generation = 0; 
		do {
			this.simulationView.getTextViewingArea().append("\nGeneración: " + generation + "\n");
			this.simulationView.showSimulation(this);
			this.world.updateGrid();
			generation++;
		}
		while (generation <= this.cycles);
	}
	
	public Simulation getSimulation() {
		return this.simulation;
	}
	
	public World getWorld() {
		return this.world;
	}
	
} 
