package entitys;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MailTest {

	private static Mail mailTest1;
	private Mail mailTest2;

	/**
	 * Método que se ejecuta una sola vez al principio del conjunto pruebas.
	 */
	@BeforeAll
	public static void iniciarlizeGlobalData() {
		try {
			mailTest1 = new Mail("nombre@gmail.com");
		} 
		catch (EntitysException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método que se ejecuta una sola vez al final del conjunto pruebas. No es
	 * necesario en este caso.
	 */
	@AfterAll
	public static void clearGlobalData() {
		mailTest1 = null;
	}

	/**
	 * Método que se ejecuta antes de cada prueba.
	 */
	@BeforeEach
	public void initializeTestData() {
		mailTest2 = new Mail();
	}

	/**
	 * Método que se ejecuta después de cada prueba.
	 */
	@AfterEach
	public void clearTestData() {
		mailTest2 = null;
	}

	// Test's con DATOS VALIDOS

	@Test
	public void testSetText() {
		this.mailTest2.setText("DAVID@GMAIL.COM");
		assertEquals(mailTest2.getText(),"DAVID@GMAIL.COM");
	}

	// Test con DATOS NO VALIDOS

	@Test
	public void testSetTextNotValid() {
		try {
			mailTest2.setText("pepe#gmail.com");
			fail("No debe llegar aqu�...");
		} 
		catch (EntitysException e) {		
		}
	}

	@Test
	public void testSetTextWhite() {
		try {
			mailTest2.setText("  ");
			fail("No debe llegar aquí...");
		} 
		catch (EntitysException e) {		
		}
	}

	@Test
	public void testSetTextNull() {
		try {
			mailTest2.setText(null);
			fail("No debe llegar aquí...");
		} 
		catch (EntitysException e) {		
		}
	}
	@Test 
	public void equalTest() {
		Mail mail1 = new Mail ("userd1@gmail.com");
		Mail mail2 = new Mail ("userd2@gmail.com");
		Mail mail3 = new Mail ("");
		assertTrue(mail1.equals(mail1));
		assertFalse(mail1.equals(mail2));
		assertFalse(mail1.equals(mail3));
	}
	@Test
	public void cloneTest() {
		Mail cloned = mailTest1.clone();
		assertEquals(mailTest1.getText(), cloned.getText());
		assertTrue(mailTest1.equals(cloned));
	}

} 
